package split.wise.web.service;

import split.wise.web.model.Expense;
import split.wise.web.model.Users;

import java.util.List;

public interface ExpenseService {

    Expense save(Expense expense);

    Expense get(Long id);

    List<Expense> getAll();

    List<Expense> getAllByAddedUser(Users user);

    List<Expense> getAllUserContributingExpenses(Users user);

}
