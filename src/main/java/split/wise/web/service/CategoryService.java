package split.wise.web.service;

import split.wise.web.model.Category;

public interface CategoryService {

    void save(Category category);

}
