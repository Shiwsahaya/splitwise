package split.wise.web.service;

import split.wise.web.model.Expense;
import split.wise.web.model.Repayments;
import split.wise.web.model.Users;

public interface RepaymentsService {

    Repayments save(Repayments repayment);

    Repayments save(Expense expense, Users fromUser, Users toUser, Long amount);

}
