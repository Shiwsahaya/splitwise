package split.wise.web.service;

import split.wise.web.model.ExpenseDebts;

public interface ExpenseDebtsService {

    ExpenseDebts save(ExpenseDebts expenseDebts);

}
