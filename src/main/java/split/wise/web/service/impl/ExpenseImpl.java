package split.wise.web.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import split.wise.web.model.Expense;
import split.wise.web.model.Users;
import split.wise.web.repository.ExpenseRepository;
import split.wise.web.service.ExpenseService;

import java.util.List;


@Service
public class ExpenseImpl implements ExpenseService {

    @Autowired
    private ExpenseRepository expenseRepo;

    public Expense save(Expense expense){
        return expenseRepo.save(expense);
    }

    public List<Expense> get(){
        return expenseRepo.findAll();
    }

    @Override
    public Expense get(Long id) {
        return expenseRepo.getOne(id);
    }

    @Override
    public List<Expense> getAll() {
        return expenseRepo.findAll();
    }

    @Override
    public List<Expense> getAllByAddedUser(Users user) {
        return expenseRepo.findByAddedBy(user);
    }

    @Override
    public List<Expense> getAllUserContributingExpenses(Users user) {
        return expenseRepo.findAllByContributorsContaining(user);
    }

}
