package split.wise.web.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import split.wise.web.model.ExpenseDebts;
import split.wise.web.repository.ExpenseDebtsRepository;
import split.wise.web.service.ExpenseDebtsService;


@Service
public class ExpenseDebtsImpl implements ExpenseDebtsService {

    @Autowired
    private ExpenseDebtsRepository expenseDebtsRepository;

    public ExpenseDebts save(ExpenseDebts expenseDebts){
        return expenseDebtsRepository.save(expenseDebts);
    }

}
