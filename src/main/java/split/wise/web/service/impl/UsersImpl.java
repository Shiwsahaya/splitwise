package split.wise.web.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import split.wise.web.model.Debt;
import split.wise.web.model.Users;
import split.wise.web.repository.UsersRepository;
import split.wise.web.service.DebtService;
import split.wise.web.service.UsersService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


@Service
public class UsersImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private DebtService debtService;

    public void save(Users users){
       usersRepository.save(users);
    }

    public Users get(Long id){
        Optional<Users>result= usersRepository.findById(id);
        return result.get();
    }

    @Override
    public Users get(String email) {
        return usersRepository.findByEmail(email);
    }

    @Override
    public List<Users> getAll() {
        return usersRepository.findAll();
    }

    @Override
    public void makeFriends(Users user1, Users user2) {
        System.out.println("a");
        if(user1.getFriends() != null) {
            System.out.println("b");
            List<Users> friends = new ArrayList<>(user1.getFriends());
            System.out.println("c");
            friends.add(user2);
            System.out.println("d");
            user1.setFriends(friends);
            System.out.println("e");
        } else {
            System.out.println("f");
            user1.setFriends(Collections.singletonList(user2));
            System.out.println("g");
        }
        debtService.save(null, user1, user2);
        System.out.println("h");
        usersRepository.save(user1);
        System.out.println("i");
    }

}
