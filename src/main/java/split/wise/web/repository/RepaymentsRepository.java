package split.wise.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import split.wise.web.model.Repayments;

public interface RepaymentsRepository extends JpaRepository<Repayments, Long> {
}
