package split.wise.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import split.wise.web.model.ExpenseDebts;

public interface ExpenseDebtsRepository extends JpaRepository<ExpenseDebts,Long> {
}
