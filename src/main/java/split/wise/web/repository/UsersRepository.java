package split.wise.web.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import split.wise.web.model.Users;

public interface UsersRepository  extends JpaRepository<Users, Long> {

    Users findByEmail(String email);

}
