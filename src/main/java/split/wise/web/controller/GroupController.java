package split.wise.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import split.wise.web.model.*;
import split.wise.web.service.*;


@RestController
public class GroupController {

    @Autowired
    GroupService service;

    @Autowired
    UsersService usersService;

    @Autowired
    GroupBalanceService groupBalanceService;

    @Autowired
    DebtService debtService;

    @Autowired
    StackOverflowResolverService jsonService;

    @GetMapping("/group/{id}")
    public ResponseEntity<?> getGroup(@PathVariable Long id) {
        return new ResponseEntity<>(jsonService.resolveGroup(service.get(id)), HttpStatus.OK);
    }

    @GetMapping("/groups")
    public ResponseEntity<?> getAllGroup(){
        return new ResponseEntity<>(jsonService.resolveGroupList(service.getAll()), HttpStatus.OK);
    }

    @PostMapping("/group")
    public ResponseEntity<?> group(@RequestBody Group group) {
        Group group1 = service.save(group);
        Users creator = group1.getCreatedBy();
        group1.getMembers().forEach((member) -> {
            System.out.println(1);
//            if (!(member.getId().equals(creator.getId()))) {
//                System.out.println(2);
//                if(debtService.get(creator, usersService.get(member.getId()), null) == null) {
//                    System.out.println(3);
//                    usersService.makeFriends(usersService.get(creator.getId()), usersService.get(member.getId()));
//                    System.out.println(4);
//                    usersService.makeFriends(usersService.get(member.getId()), usersService.get(creator.getId()));
//                    System.out.println(5);
//                }
//            }
            groupBalanceService.save(member, group1);
            System.out.println(6);
            group1.getMembers().forEach((teammate) -> {
                System.out.println(7);
                if(!(member.getId().equals(teammate.getId())))
                    debtService.save(group1, member, teammate);
                System.out.println(8);
            });
        });
        return new ResponseEntity<>("group added", HttpStatus.OK);
    }

}
