package split.wise.web.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import split.wise.web.model.Users;
import split.wise.web.service.StackOverflowResolverService;
import split.wise.web.service.UsersService;


@RestController
@RequestMapping("/user")
public class SplitController {

    @Autowired
    private UsersService service;

    @Autowired
    private StackOverflowResolverService jsonService;

    @PostMapping
    public ResponseEntity<?> newUser(@RequestBody Users users) {
        service.save(users);
        return new ResponseEntity<>("new user added", HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getAllUsers() {
        return new ResponseEntity<>(jsonService.resolveUsersList(service.getAll()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUser(@PathVariable Long id) {
        return new ResponseEntity<>(jsonService.resolveUser(service.get(id)), HttpStatus.OK);
    }

    @PostMapping("/friend")
    public ResponseEntity<?> newFriend(@RequestBody Users friend) {
        service.makeFriends(service.get(1L), service.get(friend.getEmail()));
        service.makeFriends(service.get(friend.getEmail()), service.get(1L));
        return new ResponseEntity<>("new friend added", HttpStatus.OK);
    }

}

