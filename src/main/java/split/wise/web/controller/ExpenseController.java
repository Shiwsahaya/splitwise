package split.wise.web.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import split.wise.web.model.*;
import split.wise.web.service.*;

import split.wise.web.model.Category;
import split.wise.web.model.Expense;
import split.wise.web.service.CategoryService;
import split.wise.web.service.DebtService;
import split.wise.web.service.ExpenseDebtsService;
import split.wise.web.service.ExpenseService;
import split.wise.web.model.Users;
import java.util.List;

@RestController
public class ExpenseController {

    @Autowired
    ExpenseService service;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ExpenseDebtsService expenseDebtsService;

    @Autowired
    DebtService debtService;

    @Autowired
    GroupBalanceService groupBalanceService;

    @Autowired
    RepaymentsService repaymentsService;

    @Autowired
    UsersService usersService;

    @Autowired
    StackOverflowResolverService jsonService;

    @PostMapping("/category")
    public ResponseEntity<?> addCategory(@RequestBody Category category) {
        categoryService.save(category);
        return new ResponseEntity<>("category added", HttpStatus.OK);
    }

    @PostMapping("/expense")
    public ResponseEntity<?> addExpense(@RequestBody Expense expense) {
        Expense expense1 = service.save(expense);
        if(expense1.getGroup() != null) {
            expense1.getContributors().forEach((contributor -> {
                GroupBalance groupBalance = groupBalanceService.get(contributor.getUser(), expense1.getGroup());
                groupBalance.setAmount(groupBalance.getAmount() + contributor.getNetBalance());
                groupBalanceService.save(groupBalance);
                contributor.setExpense(expense1);
                expenseDebtsService.save(contributor);
            }));
            expense1.getRepayments().forEach((repayment -> {
                Debt debt1 = debtService.get(repayment.getFrom(), repayment.getTo(), expense1.getGroup());
                debt1.setAmount(debt1.getAmount() + repayment.getAmount());
                debtService.save(debt1);
                Debt debt2 = debtService.get(repayment.getTo(), repayment.getFrom(), expense1.getGroup());
                debt2.setAmount(debt2.getAmount() - repayment.getAmount());
                debtService.save(debt2);
                repayment.setExpense(expense1);
                repaymentsService.save(repayment);
            }));
        }
        return new ResponseEntity<>("Expense Added", HttpStatus.OK);
    }

    @GetMapping("expense/{id}")
    public @ResponseBody ResponseEntity<?> getExpenseById(@PathVariable Long id){
        return new ResponseEntity<>(jsonService.resolveExpense(service.get(id)), HttpStatus.OK);
    }

    @GetMapping("expense/")
    public @ResponseBody ResponseEntity<?> getUserExpenses(){
        List<Expense>expenseList = service.getAllUserContributingExpenses(usersService.get(1L));
        return new ResponseEntity<>(jsonService.resolveExpenseList(expenseList), HttpStatus.OK);
    }

}
